USE [LPFCalculator]
GO
/****** Object:  StoredProcedure [dbo].[getFullLPFCalculation]    Script Date: 23/03/2017 8:41:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getFullLPFCalculation]
@channel varchar(255),
@product varchar(255),
@productType varchar(255),
@totalLAmountE float,
@propertyValE float,
@lpfPaymethod varchar(255)
AS
BEGIN

DECLARE @totalLAmount float
DECLARE @netPALpfAdded float
DECLARE @lpfPct float
DECLARE @initialLvr float
DECLARE @finalLvr float
DECLARE @lpfValue float
DECLARE @isLPFAvailable int

/* Calculation Start */

/*initial lvr calc
 Initial LVR = (Total Amount Entered/ Property Value entered)       --- (total amount entered, property value entered should be > 0)
*/
set @initialLvr = (@totalLAmountE/@propertyValE)

/*
  LPF Percentage,  LPF Value and IS LPF Available calc

  LPF Percentage = select lpf_pct from lpf_calc_data table where product, product type and channel matches the input data and (total loan amount entered is in between amount_from and amount_to) and (Initial LVR is in between lvr_from_pct and lvr_to_pct)
  LPF Value = (Total Loan Amount Entered * LPF Percentage)
  IS LPF Available = select lpf_available from lpf_calc_data table where product, product type and channel matches the input data and (total loan amount entered is in between amount_from and amount_to) and (Initial LVR is in between lvr_from_pct and lvr_to_pct)
*/
set @lpfPct = (SELECT  lpf_pct FROM  lpf_calc_data where (product = @product and product_type=@productType and channel=@channel and (amount_from <= @totalLAmountE AND amount_to >= @totalLAmountE) and (lvr_from_pct < @initialLvr AND lvr_to_pct >= @initialLvr)))
set @lpfValue = (@totalLAmountE * (@lpfPct/100))
set @isLPFAvailable = (SELECT  lpf_available FROM  lpf_calc_data where (product = @product and product_type=@productType and channel=@channel and (amount_from <= @totalLAmountE AND amount_to >= @totalLAmountE) and (lvr_from_pct < @initialLvr AND lvr_to_pct >= @initialLvr)))

/*
   Total Loan Amount, Net Proceeds After LPF is added calculation based on the LPF Payment method type selection
   if ( LPF Payment method type == 'Added to Loan') Then

       Total Loan Amount = ( Total Loan Amount Entered + LPF Value)
	   Net Proceeds After LPF is added = Total Loan Amount Entered

   End If

   if ( LPF Payment method type == 'Deducted From Loan') Then
      
	  Total Loan Amount = Total Loan Amount Entered
	   Net Proceeds After LPF is added = (Total Loan Amount Entered - LPF Value)

   End If

*/

IF @lpfPaymethod = 'Added to Loan'
BEGIN
SET @totalLAmount = (@totalLAmountE + @lpfValue)
SET @netPALpfAdded = @totalLAmountE
END

IF @lpfPaymethod = 'Deducted From Loan'
BEGIN
SET @totalLAmount = @totalLAmountE
SET @netPALpfAdded = (@totalLAmountE - @lpfValue)
END

/* 
   Final LVR Calculation

   Final LVR = (Total Loan Amount/ Property Value Entered)

*/

set @finalLvr = (@totalLAmount/@propertyValE)


SELECT convert(decimal(10, 2),(@initialLvr * 100)) as 'initial_lvr', convert(decimal(10, 2),(@finalLvr*100)) as 'final_lvr', convert(decimal(10, 2),@totalLAmount) as 'total_loan_amount', convert(decimal(10, 2),@netPALpfAdded) as 'net_proceeds_after_lpf_added', convert(decimal(10, 2),@lpfValue) as 'lpf_calculated', @isLPFAvailable as 'is_lpf_available'

END

GO
