﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using LPFCalcInterationBot.Models.DataObjects;
using LPFCalcInterationBot.Models.BLL;

namespace LPFCalcInterationBot.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var activity = await result as Activity;

            // calculate something for us to return
            int length = (activity.Text ?? string.Empty).Length;

            LPFCalcModel lpfModel = new LPFCalcModel();
            String c = "Retail";

            lpfModel.channel = c;

            // Based on the channel set the product names - Aussie (Prime, Prime Plus) all the other (Essential, Essential Plus)
            if (lpfModel.channel == "Aussie")
                lpfModel.product = "Prime";
            else
                lpfModel.product = "Essential";

            lpfModel.product_type = "Alt Doc";
            lpfModel.loan_amount = 550000.00;
            lpfModel.property_value = 700000.00;
            lpfModel.lpf_payment_method = "Added to Loan";
            lpfModel.loan_purpose = "Refinance";
            LPFCalcBLL lpfBLL = new LPFCalcBLL();
            //l4netlog.Debug("Input Params  Channel--> " + lpfModel.channel + "  Loan Purpose" + lpfModel.loan_purpose + "  Product" + lpfModel.product + "  Product Type" + lpfModel.product_type + "  Loan Amount" + lpfModel.loan_amount + "  Payment Method" + lpfModel.lpf_payment_method);
            // calculate policy max lvr
            lpfModel.policy_max_lvr = lpfBLL.getPolicyMaxLVR(lpfModel.channel, lpfModel.loan_purpose, lpfModel.product_type);
            // compute calculation to recieve other parameters
            getFullLPFCalculation_Result result1 = lpfBLL.performLPFCalculation(lpfModel.channel, lpfModel.product, lpfModel.product_type, lpfModel.loan_amount, lpfModel.property_value, lpfModel.lpf_payment_method);

            lpfModel.initial_lvr = result1.initial_lvr;
            lpfModel.final_lvr = result1.final_lvr;
            // assign the final LVR  exceeding message
            if ((double)lpfModel.final_lvr > lpfModel.policy_max_lvr)
            {
                lpfModel.error_message = "Final LVR is greater than Policy Max LVR";
            }
            else
            {
                lpfModel.error_message = "";
            }
            lpfModel.is_lpf_available = result1.is_lpf_available;
            lpfModel.lpf_calculated = result1.lpf_calculated;
            lpfModel.net_proceeds_after_lpf_added = result1.net_proceeds_after_lpf_added;
            lpfModel.total_loan_amount = result1.total_loan_amount;
            lpfModel.maxLAmount_message = "";
            // return our reply to the user
            await context.PostAsync($"You sent {activity.Text} which was {length} characters \r\n" + "Here is the default LPF Calculation for you " + "\r\n Loan Purpose : " + lpfModel.loan_purpose + "\r\n Product Type : " + lpfModel.product_type + "\r\n Channel : " + lpfModel.channel + "\r\n Product : " + lpfModel.product + "\r\n Loan Amount : " + lpfModel.loan_amount + "\r\n Property Value : " + lpfModel.property_value + "\r\n LPF Payment Method : " + lpfModel.lpf_payment_method + "\r\n Policy Max LVR : " + lpfModel.policy_max_lvr + "\r\n Initial LVR : " + lpfModel.initial_lvr + "\r\n Final LVR : " + lpfModel.final_lvr + "\r\n Total Loan Amount : " + lpfModel.total_loan_amount + "\r\n Net Proceeds After LPF is Added : " + lpfModel.net_proceeds_after_lpf_added + "\r\n LPF Calculated : " + lpfModel.lpf_calculated + "\r\n Is LPF Available : " + lpfModel.is_lpf_available);

            context.Wait(MessageReceivedAsync);
        }
    }
}