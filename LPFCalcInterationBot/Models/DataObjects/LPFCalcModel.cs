﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPFCalcInterationBot.Models.DataObjects
{
    public class LPFCalcModel
    {
        public String borrower_name { get; set; }
        public String loan_purpose { get; set; }
        public String product_type { get; set; }
        public String channel { get; set; }
        public String product { get; set; }
        public Nullable<double> loan_amount { get; set; }
        public Nullable<double> property_value { get; set; }
        public String lpf_payment_method { get; set; }
        public Nullable<double> policy_max_lvr { get; set; }
        public Nullable<decimal> initial_lvr { get; set; }
        public Nullable<decimal> final_lvr { get; set; }
        public Nullable<decimal> total_loan_amount { get; set; }
        public Nullable<decimal> net_proceeds_after_lpf_added { get; set; }
        public Nullable<decimal> lpf_calculated { get; set; }
        public Nullable<int> is_lpf_available { get; set; }
        public String error_message { get; set; }
        public String maxLAmount_message { get; set; }
    }
}