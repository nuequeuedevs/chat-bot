﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Infrastructure;
using LPFCalcInterationBot.Models.DataObjects;

namespace LPFCalcInterationBot.Models.BLL
{
    public class LPFCalcBLL
    {
        private static readonly log4net.ILog l4netlog = log4net.LogManager.GetLogger(typeof(LPFCalcBLL));
        public getFullLPFCalculation_Result performLPFCalculation(string channel, string product, string productType, Nullable<double> totalLAmountE, Nullable<double> propertyValE, string lpfPaymethod)
        {
            getFullLPFCalculation_Result lpfResult = null;
            try
            {
                l4netlog.Debug(" Start BLL : performLPFCalculation");
                // This block of code retrieves the calculation result from by calling the getFullLPFCalculation stored procedure 
                using (LPFCalculatorEntities context = new LPFCalculatorEntities())
                {
                    ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 10800;
                    IEnumerable<getFullLPFCalculation_Result> resultList = context.getFullLPFCalculation(channel, product, productType, totalLAmountE, propertyValE, lpfPaymethod).ToList();
                    l4netlog.Debug(" Inside BLL : Results count from DB ---> " + resultList.Count());
                    if (resultList.Count() > 0)
                    {
                        foreach (getFullLPFCalculation_Result element in resultList)
                        {
                            lpfResult = element;
                            l4netlog.Debug(" End BLL :  Actual data returned from db ---> Initial LVR : " + lpfResult.initial_lvr + "Final LVR : " + lpfResult.final_lvr + "LPF Calculated : " + lpfResult.lpf_calculated + "Is LPF Available : " + lpfResult.is_lpf_available + "Net Proceeds After LPF is added : " + lpfResult.net_proceeds_after_lpf_added + "Total Loan Amount : " + lpfResult.total_loan_amount);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                l4netlog.Debug(" End BLL : Exception in performLPFCalculation ---> " + ex.StackTrace);
            }



            return lpfResult;
        }

        public double getPolicyMaxLVR(string channel, string loanPurpose, string productType)
        {
            l4netlog.Debug(" Start BLL : getPolicyMaxLVR");
            // This function calculate and return the max lvr value based on the channel, loanPurpose and productType param combinations
            //default to Alt Doc lvr value
            double pmaxLvr = 80.00;
            if (productType == "Full Doc")
            {
                if (channel == "Retail")
                {
                    pmaxLvr = 85.00;
                }
                else
                {
                    if (loanPurpose == "Purchase")
                    {
                        pmaxLvr = 95.00;
                    }
                    else
                    {
                        pmaxLvr = 90.00;
                    }
                }
            }

            l4netlog.Debug(" End BLL : getPolicyMaxLVR  Max Lvr value Returned ---> " + pmaxLvr);

            return pmaxLvr;
        }
    }
}